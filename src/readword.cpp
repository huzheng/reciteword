#include "readword.h"
#include "reciteword.h"
#include "configfile.h"
#include <cstring>


#if defined(_WIN32) || defined(CONFIG_DARWIN)
#else
#include <espeak/speak_lib.h>
#endif

extern CReciteWord* g_pReciteWord;
extern ConfigFile *usercfgfile;


Cwyabdc::Cwyabdc ()
{
#ifdef G_OS_WIN32
#ifdef _MSC_VER
	pVoice = NULL;
#endif

	datapath = reciteword_data_dir + std::string(G_DIR_SEPARATOR_S "WyabdcRealPeopleTTS");
	havedatafile = g_file_test(datapath.c_str(), G_FILE_TEST_EXISTS);
	if (!havedatafile) {
		datapath = "C:\\Program Files\\WyabdcRealPeopleTTS";
		havedatafile = g_file_test(datapath.c_str(), G_FILE_TEST_EXISTS);
		if (!havedatafile)
			datapath.clear();
	}
#else
	havedatafile = g_file_test("/usr/share/WyabdcRealPeopleTTS", G_FILE_TEST_EXISTS);
	if (!havedatafile)
		g_print("RealPeopleTTS files not found!\n");
#endif
}

Cwyabdc::~Cwyabdc ()
{
#ifdef G_OS_WIN32
#ifdef _MSC_VER
	if (pVoice)
		pVoice->Release();
	::CoUninitialize();
#endif
#elif defined(CONFIG_DARWIN)
#else
	espeak_Terminate();
#endif
}

void Cwyabdc::init()
{
#ifdef G_OS_WIN32
#ifdef _MSC_VER
	if (FAILED(::CoInitialize(NULL)))
		return;
	HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
	if(!SUCCEEDED(hr)) {
		::CoUninitialize();
		return;
	}
	gchar *text;
	if (rw_cfg_read_string (usercfgfile, "sapi_tts", "voice", &text)) {
		if (text[0] != '\0') {
			DWORD dwNum = MultiByteToWideChar(CP_UTF8, 0, text, -1, NULL, 0);
			wchar_t *pwText;
			pwText = new wchar_t[dwNum];
			MultiByteToWideChar(CP_UTF8, 0, text, -1, pwText, dwNum);
			ISpObjectToken *pToken;
			if (SpGetTokenFromId(pwText , &pToken) == S_OK) {
				pVoice->SetVoice(pToken);
				pToken->Release();
			}
			delete []pwText;
		}
		g_free(text);
	}
	gboolean readok;
	USHORT volume;
	readok = rw_cfg_read_int(usercfgfile, "sapi_tts", "volume", &volume);
	if (!readok)
		volume = 100;
	pVoice->SetVolume(volume);
	long rate;
	readok = rw_cfg_read_int(usercfgfile, "sapi_tts", "rate", &rate);
	if (!readok)
		rate = 0;
	pVoice->SetRate(rate);
#endif
#elif defined(CONFIG_DARWIN)
#else
	espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
	gchar *ab;
	if (rw_cfg_read_string (usercfgfile, "reciteword", "espeak_voice_engine", &ab)) {
		espeak_voice_engine = ab;
		espeak_SetVoiceByName(ab);
		g_free(ab);
	}
#endif
}

void Cwyabdc::synth(const char *text)
{
#ifdef G_OS_WIN32
#ifdef _MSC_VER
	DWORD dwNum = MultiByteToWideChar(CP_UTF8, 0, word, -1, NULL, 0);
	wchar_t *pwText;
	pwText = new wchar_t[dwNum];
	MultiByteToWideChar(CP_UTF8, 0, word, -1, pwText, dwNum);
	pVoice->Speak(pwText, SPF_ASYNC | SPF_PURGEBEFORESPEAK | SPF_IS_NOT_XML, NULL);
	delete []pwText;
#endif
#elif defined(CONFIG_DARWIN)
#else
	espeak_Synth(text, strlen(text)+1, 0, POS_CHARACTER, 0, espeakCHARS_UTF8, NULL, NULL);
#endif
}

void Cwyabdc::read(const char *word,PLAY_METHOD method)
{
	gboolean do_synth = TRUE;
	if (havedatafile) {
		int n=strlen(word);
		gchar *lowerword = (gchar *)g_malloc(n+1);
		for (int i=0;i<n;i++)
			lowerword[i]= g_ascii_tolower(word[i]);
		lowerword[n] = '\0';
#ifdef G_OS_WIN32		
		gchar *filename = g_strdup_printf("%s\\%c\\%s.wav", datapath.c_str(), lowerword[0], lowerword);
#else
		gchar *filename = g_strdup_printf("/usr/share/WyabdcRealPeopleTTS/%c/%s.wav", lowerword[0],lowerword);
#endif		
		if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
			play_file (filename,method);
			do_synth = FALSE;
		}
		g_free(filename);
		g_free(lowerword);
	}
	if (do_synth) {
		synth(word);
	}
}

// if use global static class,the class's constructor will run before main(),it means that the constructor run before gtk_init(),
// so,if use any gtk,glib function,i.e. g_printf(),it will make some strange things.it is better not to use global static class
// in gtk program.

//static Cwyabdc wyabdc;  

void readword (const char *word,PLAY_METHOD method)
{
	if (g_pReciteWord->wyabdc.disable)
		return;
	//if (word && word[0]!='\0' && (!strchr(word,' '))) // if the string contain a space character,then no sound.
	if (word && word[0]!='\0')
		g_pReciteWord->wyabdc.read (word,method);
}
