#ifndef __RW_SNDSERV_H__
#define __RW_SNDSERV_H__

#include <glib.h>
#include <string>

enum PLAY_METHOD {PM_MIX,PM_STOP_PRE,PM_AFTER_PRE};

class CSndserv
{
#ifndef G_OS_WIN32
	//int fd;
#endif

      public:
	gboolean disable;
	bool canplay;
#ifndef G_OS_WIN32
	std::string playcmd;
	//gboolean always_use_sound_play_command;
	//void update_always_use_sound_play_command();
#endif
	  CSndserv ();
	 ~CSndserv ();
	void end();
	void init();
	void play (const gchar *filename);
};

void play_file(const gchar *filename,PLAY_METHOD method);

#endif
