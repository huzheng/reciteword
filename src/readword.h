#ifndef __RW_READWORD_H__
#define __RW_READWORD_H__

#include "sndserv.h"
#include <string>

#ifdef _WIN32
#ifdef _MSC_VER

#include <windows.h>
#define _ATL_APARTMENT_THREADED
#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>
#include <sapi.h>
#include <sphelper.h>

#endif
#endif

class Cwyabdc
{
public:
	gboolean disable;	
	Cwyabdc ();
	~Cwyabdc ();
	void init();
	void read(const char *word,PLAY_METHOD method);
	void synth(const char *text);
#ifdef G_OS_WIN32
#ifdef _MSC_VER
	ISpVoice * pVoice;
#endif
#else
	std::string espeak_voice_engine;
#endif
private:
	bool havedatafile;
#ifdef G_OS_WIN32
	std::string datapath;
#endif
};

void readword (const char *word,PLAY_METHOD method=PM_MIX);

#endif
