#include "sndserv.h"
#include "reciteword.h"
#include <cstdio>
#include <cstdlib>

#ifdef G_OS_WIN32
    #include "windows.h"
#elif defined(CONFIG_DARWIN)
#else
//	#include <esd.h>
#endif

extern CReciteWord* g_pReciteWord;

CSndserv::CSndserv()
{
#ifdef G_OS_WIN32
#else
	//fd = -1;
#endif
}

CSndserv::~CSndserv()
{
	end();
}

void CSndserv::end()
{
#ifdef G_OS_WIN32
#elif defined(CONFIG_DARWIN)
#else
/*
	if (fd>=0) {
		esd_close(fd);
		fd = -1;
	}
*/
#endif
}

void CSndserv::init()
{
#ifdef G_OS_WIN32
	canplay = true;
#elif defined(CONFIG_DARWIN)
	if (always_use_sound_play_command) {
		canplay = true;
	} else {
		// Need future work...
		canplay = false;
	}
#else
	//if (always_use_sound_play_command) {
	if (TRUE) {
		canplay = true;
	} else {
		/*fd=esd_open_sound(NULL); //"localhost";
		canplay = (fd>=0);
		if (!canplay)
			g_print("Esd initialization failed, no sound will be play!\n");
		*/
	}
#endif
}

#ifndef G_OS_WIN32
/*
void CSndserv::update_always_use_sound_play_command()
{
	end();
	init();
}
*/
#endif

void
CSndserv::play(const gchar *filename)
{
#ifdef G_OS_WIN32
#elif defined(CONFIG_DARWIN)
#else
	/*if (fd>=0) {
		esd_play_file(NULL,filename,0); // ???
		//esd_play_file ("reciteword", filename, 1);
	}*/
#endif
}

//static CSndserv sndserv;

#ifndef G_OS_WIN32
/*
static gpointer play_file_mix(gpointer data)
{
	g_pReciteWord->sndserv.play((gchar *)data);
	g_free(data);
	return NULL;
}
*/
#endif

/*
static void play_file_after_pre(gpointer data,gpointer user_data)
{
	g_pReciteWord->sndserv.play((gchar *)user_data);
	return NULL;
}

class CThreadPool
{
	GThreadPool *pool;
public:
	CThreadPool();
	~CThreadPool();
};

CThreadPool::CThreadPool()
{
	pool = g_thread_pool_new(play_file_after_pre,NULL,1,FALSE,NULL);
}

CThreadPool::~CThreadPool()
{
	g_thread_pool_free(pool,);
}
*/


void play_file(const char *filename,PLAY_METHOD method)
{
	if ((!g_pReciteWord->sndserv.canplay)||(g_pReciteWord->sndserv.disable))
		return;

	//esd_play_file ("reciteword", filename, 1);  //it can't return quickly :( when in typing,press wrong key will cause freezing.
		
	// the system() function is inefficiency, should change to use thread to call esd_play_file().
	/*gchar command[256];
	sprintf(command,"esdplay %s &",filename);
	system(command);*/
	
	//I tried to use gnome_sound_play() without threading, but it is too slow when you type words fast.
	
	switch (method)
	{
		case PM_MIX:
		{
#ifdef G_OS_WIN32
			PlaySound(filename, 0, SND_ASYNC | SND_FILENAME);
#else
			//if (g_pReciteWord->sndserv.always_use_sound_play_command) {
			if (TRUE) {
				gchar *efilename = g_shell_quote(filename);
				gchar *command = g_strdup_printf("%s %s", g_pReciteWord->sndserv.playcmd.c_str(), efilename);
				g_free(efilename);
				gboolean result = g_spawn_command_line_async(command, NULL);
				if (!result) {
					g_print("run command error!\n");
				}
				g_free(command);
			} else {
/*
				gchar *dup_filename = g_strdup(filename); //as in the new thread, filename may have already be freeed.
				g_thread_unref(g_thread_new("play_file_mix", play_file_mix, (gpointer)dup_filename));  //use GThreadPool may be more efficient.
*/
			}
#endif
			break;
		}
		case PM_STOP_PRE:   // it is hard to done,may need write my own esd_play_file.
		{
			break;
		}
		case PM_AFTER_PRE:  // use GThreadPool can do this,but,it does seems very useful before PM_STOP_PRE is done.
		{
			//GThreadPool *threadpool;
			
			break;
		}
	}
}
