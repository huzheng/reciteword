Name: reciteword
Summary: Recite Word Easily
Version: 0.8.8
Release: 1%{?dist}
Group: Applications/Productivity
License: GPLv3
URL: http://reciteword.sourceforge.net
Source: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gtk2
Requires: espeak
BuildRequires: gettext, gtk2-devel, espeak-devel, perl-XML-Parser

%description
ReciteWord is an education software to help people to study English,
recite words.

%description -l zh_CN
黑客背单词。

%prep
%setup -q

%build
%configure CFLAGS="-g -O2" CXXFLAGS="-g -O2"
make -k

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-, root, root, -)
%{_bindir}/reciteword
%{_datadir}/reciteword
%{_datadir}/applications/*
%{_datadir}/pixmaps/*
