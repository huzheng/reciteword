﻿reciteword-0.8.8 黑客背单词完全版
http://reciteword.sourceforge.net

作者：胡正 <huzheng001@gmail.com> http://www.huzheng.org

这是一个来自Linux下的自由软件，遵循GPL协议3.0或以后版本。

提示：
1.你可以到ReciteWord主页下载真人语音库文件WyabdcRealPeopleTTS.tar.bz2，然后用WinRAR解压后得到WyabdcRealPeopleTTS目录，将其移到reciteword.exe所在目录或者C:\Program Files\下，这样背单词时就可以听到单词读音了。

2.ReciteWord在安装后会在开始菜单和桌面建立快捷方式，如果你手工创建快捷方式，请把快捷方式的起始位置设为"C:\Program Files\GTK2-Runtime\bin"，否则会出现运行时停止响应的现象。

2021.9.19
胡正
