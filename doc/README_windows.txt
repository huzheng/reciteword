You can read the dict/doc/README.win32 file of stardict for detail. http://stardict-4.sourceforge.net

=====
ReciteWord can be compiled and run in windows.

Please install Dev-C++, it can be found at http://sourceforge.net/projects/orwelldevcpp/
My file: "Dev-Cpp 5.11 TDM-GCC 4.9.2 Setup.exe".

Visit https://download.gnome.org/binaries/win32/gtk%2B/
Download all-in-one bundle:
gtk+-bundle_2.24.10-20120208_win32.zip
Extract it to C:\Program Files\Dev-Cpp\MinGW64\lib32\

Here is a reciteword.dev
Use Dev-C++ to compile it. Becase the compile command is too long, it can't be compile in win98, please use windows XP.

After compiled, you will find reciteword.exe at src/.

BUG: if you select the Revise-Group, then select the Typing or Shooting by main menu, reciteword will got freezed(in Shooting, you can see that gtk_timeout is still working). I find if we disable the intl feature(rename locale/*/reciteword.mo), this bug will be disappear. libintl's BUG???


========
To build the installer.

Grab and install NSIS: http://sourceforge.net/projects/nsis/
My file: nsis-3.07-setup.exe

I use Linux commmand to illustrate the steps, but you cann't do it in this way :)

cd reciteword-0.8.8
mkdir win32-install-dir
cp readme.txt src/reciteword.exe win32-install-dir
mkdir -p win32-install-dir/locale/zh_CN/LC_MESSAGES/
cp po/zh_CN.gmo win32-install-dir/locale/zh_CN/LC_MESSAGES/reciteword.mo

Copy the "dicts","modules","skins","books" directories into "win32-install-dir".
Then:
rm modules/Makefile.am modules/Makefile.in
rm skins/Makefile.am skins/Makefile.in
rm skins/bdc/Makefile.am skins/bdc/Makefile.in


This installer don't contains the gtk2-runtime, download it at: http://sourceforge.net/projects/gtk-win

mkdir gtk_installer
cp gtk2-runtime-2.24.10-2012-10-10-ash.exe gtk_installer

Right click the reciteword-installer.nsi file, choose "Compile NSIS script" will build the installer.

ReciteWord's win32 port got many experience(i.e. dll hell make reciteword freezed) from pidgin(http://pidgin.im) and StarDict(http://stardict-4.sourceforge.net).

Note: when use fopen(), use "rb", never "r", unless you know what you are doing.

HuZheng 2021.9.19
huzheng_001@hotmail.com
http://www.huzheng.org
